package com.deds.quran;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.deds.quran.databinding.ActivitySurahListviewBinding;


import java.util.ArrayList;
import java.util.List;

public class SurahListAdapter extends RecyclerView.Adapter<SurahListAdapter.ViewHolder> {

    private static final String TAG = "RecyclerAdapter";
    List<Surah> list = new ArrayList<>();


    public void updateList(List<Surah> list) {
        this.list = list;
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        ActivitySurahListviewBinding activitySurahListviewBinding;



        public ViewHolder(@NonNull ActivitySurahListviewBinding activitySurahListviewBinding) {
            super(activitySurahListviewBinding.getRoot());
            this.activitySurahListviewBinding = activitySurahListviewBinding;
            Context context = itemView.getContext();

            activitySurahListviewBinding.lib.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent sIntent = new Intent(context, surah_activity.class);
                    sIntent.putExtra("id",list.get(getAdapterPosition()).getFile_name());
                    sIntent.putExtra("name","  سُورَةُ  " +list.get(getAdapterPosition()).getSurah_name());

                    context.startActivity(sIntent);
                    Log.d(TAG, "onClick: " + list.get(getAdapterPosition()).getFile_name());
                }
            });

        }

        public void bindingView(Surah i) {
//            activitySurahListviewBinding.lib.setText(i.getPLAFOND()!=null ? String.format("%.2f", i.getPLAFOND()):"");
            activitySurahListviewBinding.lib.setText("  سُورَةُ  " +i.getSurah_name() );
            //activitySurahListviewBinding.image.setImageResource(getResources(i.getIDO()));
        }

    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        ActivitySurahListviewBinding recyclerRowBinding = ActivitySurahListviewBinding.inflate(layoutInflater, parent, false);
        return new ViewHolder(recyclerRowBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
       holder.bindingView(list.get(position));
    }


    @Override
    public int getItemCount() {
        Log.d(TAG, "getItemCount: " + list.size());
        return list.size();
    }
}
