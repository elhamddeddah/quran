package com.deds.quran;

public class Surah {
    private String file_name;
    private String surah_name;

    public Surah() {
    }

    public Surah(String file_name, String surah_name) {
        this.file_name = file_name;
        this.surah_name = surah_name;
    }

    public String getFile_name() {
        return file_name;
    }

    public void setFile_name(String file_name) {
        this.file_name = file_name;
    }

    public String getSurah_name() {
        return surah_name;
    }

    public void setSurah_name(String surah_name) {
        this.surah_name = surah_name;
    }
}
