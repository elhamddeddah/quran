package com.deds.quran;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import  com.deds.quran.databinding.ActivityMainBinding;
import com.deds.quran.databinding.ActivitySurahItemsBinding;
import com.deds.quran.databinding.ActivitySurahListviewBinding;
import com.google.gson.Gson;

import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class MainActivity extends AppCompatActivity {
    private ActivitySurahItemsBinding binding;
    private ActivitySurahListviewBinding bindingListview;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivitySurahItemsBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        String json = null;
        try {
            InputStream is = getResources().openRawResource(R.raw.all);
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            json=ex.getMessage();
            ex.printStackTrace();

        }
        System.out.println(json);
        Gson gson = new Gson(); // Or use new GsonBuilder().create();
        Map target2 = gson.fromJson(json, Map.class);
        System.out.println(target2);
        List<Surah> list = new ArrayList<>();
        for (Object key : target2.keySet()) {
            list.add(new Surah(key.toString(),target2.get(key).toString()));        }
        bindingListview = ActivitySurahListviewBinding.inflate(getLayoutInflater());
        SurahListAdapter SurahListAdapter=new SurahListAdapter();
        binding.recyclerView.setAdapter(SurahListAdapter);
        SurahListAdapter.updateList(list);
        //gen(json);
    }
    public void gen(String s){
        JSONObject  surahList= new JSONObject();
        JSONObject surah = new JSONObject();

        List<String> list =Arrays.asList(s.split("سُورَةُ "));
        try {
        for (int i = 0; i < list.size(); i++) {
            String item=list.get(i);
            if(item.contains("بِسْمِ اِ۬للَّهِ اِ۬لرَّحْمَٰنِ اِ۬لرَّحِيمِ") )
            { surah.put("name", item.substring(0,item.indexOf("بِسْمِ اِ۬للَّهِ اِ۬لرَّحْمَٰنِ اِ۬لرَّحِيمِ")).
                    replaceAll("\"",""));
                surah.put("text", item.substring(item.indexOf("بِسْمِ اِ۬للَّهِ اِ۬لرَّحْمَٰنِ اِ۬لرَّحِيمِ")).
                        replaceAll("\"",""));
                surahList.put("s"+i+".json", surah.get("name"));
                String e=surah.toString();
                FileOutputStream fos = this.openFileOutput("s"+i+".json", Context.MODE_PRIVATE);
                fos.write(e.getBytes());
                fos.close();
                Log.d(i+"JSON" , surah.toString());
            }else if (item.length()>12){
                surah.put("name", item.substring(0,12));
                surah.put("text", item.substring(12));
                surahList.put("s"+i+".json", surah.get("name"));
                String e=surah.toString();
                FileOutputStream fos = this.openFileOutput("s"+i+".json", Context.MODE_PRIVATE);
                fos.write(e.getBytes());
                fos.close();
                Log.d(i+"JSON" , surah.toString());
            }


        }
        FileOutputStream fos = this.openFileOutput("all.json", Context.MODE_PRIVATE);
        fos.write(surahList.toString().getBytes());
        fos.close();
        } catch (IOException  e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }
}