package com.deds.quran;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.deds.quran.databinding.ActivitySurah2Binding;
import com.deds.quran.databinding.ActivitySurahItemsBinding;
import com.google.gson.Gson;

import java.io.IOException;
import java.io.InputStream;
import java.util.Map;

public class surah_activity extends AppCompatActivity {
    private ActivitySurah2Binding binding;
    static int textSize = 12;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = ActivitySurah2Binding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        String json = null;
        try {
            Bundle extras = getIntent().getExtras();
            Resources resources = binding.getRoot().getResources();
            binding.lib1.setText(extras.getString("name"));
            System.out.println(extras.getString("id"));
            int next = 1 + Integer.valueOf(extras.getString("id").replace(".json", "").substring(1));
            Log.d("next", "next val: " + next);
            if (next == 115)
                binding.next.setVisibility(View.INVISIBLE);
            else {
                Surah next_surah = getNext(next);
                binding.lib.setText("  سُورَةُ  " + next_surah.getSurah_name());
                binding.lib.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent sIntent = new Intent(binding.getRoot().getContext(), surah_activity.class);
                        sIntent.putExtra("id", next_surah.getFile_name());
                        sIntent.putExtra("name", "  سُورَةُ  " + next_surah.getSurah_name());
                        binding.getRoot().getContext().startActivity(sIntent);
                    }
                });

            }

            final int resourceId = resources.getIdentifier(extras.getString("id").replace(".json", ""), "raw",
                    binding.getRoot().getContext().getPackageName());
            InputStream is = getResources().openRawResource(resourceId);
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            json = ex.getMessage();
            ex.printStackTrace();

        }
        Gson gson = new Gson(); // Or use new GsonBuilder().create();
        Map target2 = gson.fromJson(json, Map.class);
        Typeface typeface = null;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            typeface = getResources().getFont(R.font.qrfont);
        }
        //binding.textView.setTypeface(typeface);


        Log.d("TAG", "deddaha: " + binding.textView.getFontFeatureSettings());
        Log.d("TAG", "deddaha: " + Build.VERSION.SDK_INT);
        Log.d("TAG", "deddaha: " + Build.VERSION_CODES.O);
        binding.lib.setTextSize(textSize);
        binding.lib1.setTextSize(textSize);
        binding.textView.setTextSize(textSize);

        binding.textView.setText(target2.get("text").toString().replaceAll("", ""));
        binding.add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (textSize != 40)
                    textSize++;
                Log.d("textSize", "deddaha: " + textSize);
                binding.lib.setTextSize(textSize);
                binding.lib1.setTextSize(textSize);
                binding.textView.setTextSize(textSize);
            }
        });
        binding.minus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (textSize != 8)
                    textSize--;
                Log.d("textSize", "deddaha: " + textSize);
                binding.lib.setTextSize(textSize);
                binding.lib1.setTextSize(textSize);
                binding.textView.setTextSize(textSize);
            }
        });
    }

    @Override
    public void onBackPressed() {
        Intent sIntent = new Intent(binding.getRoot().getContext(), MainActivity.class);
        binding.getRoot().getContext().startActivity(sIntent);
    }

    private Surah getNext(int next) {
        String json = null;
        try {
            InputStream is = getResources().openRawResource(R.raw.all);
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            json = ex.getMessage();
            ex.printStackTrace();

        }
        Gson gson = new Gson(); // Or use new GsonBuilder().create();
        Map target2 = gson.fromJson(json, Map.class);
        return new Surah("s" + next + ".json", target2.get("s" + next + ".json").toString());

    }
}